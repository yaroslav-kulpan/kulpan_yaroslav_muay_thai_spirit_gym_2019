$(document).ready(function () {
    $('.card').hide();
    $('.card').slice(0, 3).show();
    $('.btn-show-more').on('click', function (e) {
        e.preventDefault();
        $('.card:hidden').slice(0, 3).show();
        if ($('.card:hidden').length < 1) {
            $('.btn-show-more').hide();
        }
    })
});

$('.responsive').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                writableWidth: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});